const updateUI = () => {
  document.getElementById("life").innerHTML = life;
  // document.getElementById("gold").innerHTML = gold;
  document.getElementById("demons-ghost").innerHTML = demons.ghost;
  document.getElementById("demons-imp").innerHTML = demons.imp;
  document.getElementById("demons-demon").innerHTML = demons.demon;
  document.getElementById("demons-gin").innerHTML = demons.gin;

  const star = document.querySelector(".star");
  // const borderWidth = star.offsetWidth - (life / 100d00) * 2; // Calculate border based on radius
  // star.style.borderWidth = `${borderWidth}px`; // Set the border width dynamically

  for (let i = 0; i < demonOrders.length; i++) {
    renderOrder(demonOrders[i], i);
  }
  renderCurrentSummon();
};

const renderArrows = () => {
  currentArr = 0;
  document.getElementById("arrContiainer").innerHTML = "";
  for (let i = 0; i < directionArr.length; i++) {
    document.getElementById("arrContiainer").innerHTML += `      <svg
    id="arr${i}"a
    class="${directionArr[i]}"
    xmlns="http://www.w3.org/2000/svg"
    xmlns:xlink="http://www.w3.org/1999/xlink"
    x="0px"
    y="0px"
    width="122.88px"
    height="122.433px"
    viewBox="0 0 122.88 122.433"
  >
    <g>
      <polygon
        fill="currentColor"
        clip-rule="evenodd"
        points="122.88,61.217 59.207,122.433 59.207,83.029 0,83.029 0,39.399 59.207,39.399 59.207,0 122.88,61.217"
      />
    </g>
  </svg>`;
  }
};

const renderOrder = (order, index) => {
  document.getElementById(`order${index}`).innerHTML = `
  <span>
  Order ${index + 1} 
  </span>
    <div class="container-order-demons">
      <span class="order-demons">👻:${order.ghost}</span>
      <span class="order-demons">👾:${order.imp}</span>
    </div>
    <div class="container-order-demons">
      <span class="order-demons">👺:${order.demon}</span>
      <span class="order-demons">🌪️:${order.gin}</span>
    </div>
  </div>
  `;
};

const renderCurrentSummon = () => {
  // document.getElementById("demons-ghost").classList.remove = "demon-highlight";
  // document.getElementById("demons-imp").classList.remove = "demon-highlight";
  // document.getElementById("demons-demon").classList.remove = "demon-highlight";
  // document.getElementById("demons-gin").classList.remove = "demon-highlight";

  document.getElementById("summonDesc").innerHTML =
    getSummonEmoji(currentSummon);
};

const telescopeSound = new Howl({
  src: ["./g_telescope.ogg"],
});
const pigSound = new Howl({
  src: ["./g_pig.ogg"],
});
const fireSound = new Howl({
  src: ["./fire.ogg"],
});
const pizzaSound = new Howl({
  src: ["./pizza.ogg"],
});
const candleSound = new Howl({
  src: ["./g_candle.ogg"],
});

const showAnimation = (type) => {
  document.getElementById("mage").classList.add("hidden");
  document.getElementById("telescope").classList.add("hidden");
  document.getElementById("pig").classList.add("hidden");
  document.getElementById("candle").classList.add("hidden");
  document.getElementById("pizza").classList.add("hidden");
  document.getElementById("damage").classList.add("hidden");

  switch (type) {
    case "mage":
      document.getElementById("mage").classList.remove("hidden");
      break;
    case "telescope":
    case "left":
      document.getElementById("telescope").classList.remove("hidden");
      telescopeSound.play();
      break;
    case "pig":
    case "right":
      document.getElementById("pig").classList.remove("hidden");
      pigSound.play();
      break;

    case "candle":
    case "up":
      document.getElementById("candle").classList.remove("hidden");
      candleSound.play();
      break;

    case "pizza":
    case "down":
      document.getElementById("pizza").classList.remove("hidden");
      pizzaSound.play();
      break;

    case "damage":
      document.getElementById("damage").classList.remove("hidden");
      fireSound.play();
      break;
      w;

    default:
      break;
  }
};
showAnimation("mage");
