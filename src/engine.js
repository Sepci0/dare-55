function shuffleArray(array) {
  for (var i = array.length - 1; i > 0; i--) {
    var j = Math.floor(Math.random() * (i + 1));
    var temp = array[i];
    array[i] = array[j];
    array[j] = temp;
  }
  return array;
}

const getRandomArrow = () => {
  let tempArr = ["left", "up", "right", "down"];
  return tempArr[Math.floor(Math.random() * tempArr.length)];
};

const getRandomDemonOrder = () => {
  return {
    ghost: Math.floor(Math.random() * 4),
    imp: Math.floor(Math.random() * 4),
    demon: Math.floor(Math.random() * 4),
    gin: Math.floor(Math.random() * 2),
  };
};

const getSummonEmoji = (summon) => {
  switch (summon) {
    case "ghost":
      return "👻";
    case "imp":
      return "👾";
    case "demon":
      return "👺";
    case "gin":
      return "🌪️";
    default:
      return "";
  }
};
