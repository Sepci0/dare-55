// masz krąg życia, i każdy błąd go zmniejsza.
// jeżeli nic nie przyzwiesz to on spada.

let directionArr = [];
let currentArr = 0;
let currentSummon = "ghost";
let summonIndex = 0;
let life = 100;
let gold = 0;

let demonOrders = [
  getRandomDemonOrder(),
  getRandomDemonOrder(),
  getRandomDemonOrder(),
  getRandomDemonOrder(),
];

let demons = {
  all: 0,
  // demons
  ghost: 0,
  imp: 0,
  demon: 0,
  gin: 0,
};

const addDemons = (type) => {
  demons["all"]++;
  demons[type]++;
};

const OnArrowPress = (direction) => {
  if (direction == directionArr[currentArr]) {
    document.getElementById("arr" + currentArr).classList.add("correct");
    currentArr++;
    showAnimation(direction);
    if (currentArr == directionArr.length) {
      addDemons(currentSummon);
      showAnimation("mage");
      currentArr = 0;
      setSummon(currentSummon);
    }
  } else {
    showAnimation("damage");
    Toastify({
      text: `Wrong incantation: -10 ❤️`,
      style: {
        background: "linear-gradient(to right, #cc2200, #ee0000)",
      },
      gravity: "bottom", // `top` or `bottom`
      position: "right",
    }).showToast();
    life -= 10;
    setSummon(currentSummon);
  }
};

const setSummon = (mode) => {
  let rnd1 = getRandomArrow();
  let rnd2 = getRandomArrow();
  switch (mode) {
    case 0:
    case "ghost":
      if ((currentSummon = "ghost")) {
        while (rnd1 == directionArr[0]) {
          rnd1 = getRandomArrow();
        }
      }
      currentSummon = "ghost";
      directionArr = [rnd1, rnd1, rnd1, rnd1];
      renderArrows();
      break;
    case 1:
    case "imp":
      currentSummon = "imp";
      while (rnd1 == rnd2) {
        rnd2 = getRandomArrow();
      }
      directionArr = [rnd1, rnd1, rnd2, rnd2];
      renderArrows();
      break;
    case "demon":
    case 2:
      currentSummon = "demon";
      directionArr = shuffleArray(["left", "up", "right", "down"]);
      renderArrows();
      break;
    case "gin":
    case 3:
      currentSummon = "gin";
      directionArr = [];
      for (let i = 0; i < 6; i++) {
        directionArr.push(getRandomArrow());
      }
      renderArrows();
      break;

    default:
      break;
  }
};
setSummon(currentSummon);

const returnDemonOrder = (orderNumber) => {
  let checkingOrder = demonOrders[orderNumber];
  let all = 0;
  let correct = 0;
  for (const demonKeyType in checkingOrder) {
    all++;
    if (demons[demonKeyType] >= checkingOrder[demonKeyType]) {
      correct++;
    } else {
      Toastify({
        text: `Not enough ${getSummonEmoji(demonKeyType)} `,
        style: {
          background: "linear-gradient(to right, #b99400, #b9ac00)",
        },
        gravity: "bottom", // `top` or `bottom`
        position: "left",
      }).showToast();

      return;
    }
  }
  if (correct >= all) {
    for (const demonKeyType in checkingOrder) {
      demons[demonKeyType] -= checkingOrder[demonKeyType];
      if (demons[demonKeyType] < 0) demons[demonKeyType] = 0;
    }
    demonOrders[orderNumber] = getRandomDemonOrder();
    life += 10;
  }
};

document.addEventListener("keypress", (event) => {
  switch (event.key.toLowerCase()) {
    case "a":
    case "ArrowLeft":
      OnArrowPress("left");
      break;
    case "d":
    case "ArrowRight":
      OnArrowPress("right");
      break;
    case "w":
    case "ArrowUp":
      OnArrowPress("up");
      break;
    case "s":
    case "ArrowDown":
      OnArrowPress("down");
      break;
    case "e":
      summonIndex++;
      if (summonIndex > 3) summonIndex = 3;
      if (summonIndex < 0) summonIndex = 0;
      setSummon(summonIndex);
      break;
    case "q":
      summonIndex--;
      if (summonIndex > 3) summonIndex = 3;
      if (summonIndex < 0) summonIndex = 0;
      setSummon(summonIndex);
      break;
    case "1":
    case "2":
    case "3":
    case "4":
      returnDemonOrder(Number(event.key) - 1);
      break;
    default:
      break;
  }
  updateUI();
});
updateUI();

window.dmgTicker = setInterval(() => {
  life -= 1;
  updateUI();
  if (life < 0) {
    document.getElementById("loseModal").classList.remove("hidden");
  }
}, 1000);
